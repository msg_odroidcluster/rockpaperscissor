import faulthandler; faulthandler.enable()
  

import simplejson
from kivy.app import App
from kivy.factory import Factory
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.properties import ObjectProperty
from kivy.uix.carousel import Carousel
from kivy.uix.gridlayout import GridLayout
import matplotlib.pyplot as plt
import matplotlib
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.image import Image, AsyncImage
from kivy.uix.label import Label
from kivy.uix.widget import Widget
from kivy.core.window import Window

matplotlib.use("module://kivy.garden.matplotlib.backend_kivy")
from kivy.garden.matplotlib import FigureCanvasKivyAgg
import numpy as np
import pandas as pd
import sys
import datetime
import time

import RPi.GPIO as GPIO  

sys.path.insert(0, '../backend/')
import RPSModelHandler as rps_mh

from RPSModelHandler import RPSUtils as rpsUtils

from os import listdir
import time

kv_path = './kv/'
PinRock = 12 
PinPaper = 5 
PinScissor = 6

for kv in listdir(kv_path):
    Builder.load_file(kv_path+kv)



class HistoryFigure(FigureCanvasKivyAgg):
    def __init__(self, **kwargs):
        plt.figure(1)
        plt.plot([2,1])
        super(HistoryFigure, self).__init__(plt.gcf(), **kwargs)


class ImportanceFigure(FigureCanvasKivyAgg):
    def __init__(self, **kwargs):
        plt.figure(2)
        plt.plot([1,2])
        super(ImportanceFigure, self).__init__(plt.gcf(), **kwargs)

class MyList(BoxLayout):
    pass

class RockButton(Button):
    pass


class PaperButton(Button):
    pass


class ScissorButton(Button):
    pass

class Container(Widget):
    pass

class GameScreen(Screen):
    display = ObjectProperty()
    diagram = ObjectProperty()
    counter = ObjectProperty()
    popup = ObjectProperty()
    rounds = 20
    successLimit = 10
    opponentName = ''
    playerName = ''
    rpsHandler = None
    data = []

    def initialize(self):



        self.headline.text = self.playerName + ' *** VS *** '+ self.opponentName
        filename = '../../datasets/' + self.opponentName
        # np.savetxt(filename, (xVals, yVals), delimiter=',')
        self.data = np.genfromtxt(filename, delimiter=',')

        data0 = self.data[0].astype(int)
        data1 = self.data[1].astype(int)
        # rpsHandler = rps_mh.RPSModelHandler(np.ones(150, dtype=int), np.ones(150, dtype=int), depth=5)
        self.rpsHandler = rps_mh.RPSModelHandler(data0, data1, depth=5)
        self.set_counter()

        GPIO.cleanup()
        GPIO.setmode(GPIO.BCM)       # Number GPIOs by BCM chip numbering scheme  
        GPIO.setup(PinRock, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)   # Set LedPinR mode input  
        GPIO.setup(PinPaper, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)  
        GPIO.setup(PinScissor, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(PinRock, GPIO.RISING, callback = self.play, bouncetime = 3000) 
        GPIO.add_event_detect(PinPaper, GPIO.RISING, callback = self.play, bouncetime = 3000) 
        GPIO.add_event_detect(PinScissor, GPIO.RISING, callback = self.play, bouncetime = 3000)
        

    def getLenOfInitData(self):
        #return 0
        return len(self.data[0])

    def set_counter(self):
        self.counter.text = str(self.rpsHandler.num_of_rounds() - len(self.data[0])) + ' / ' + str(self.rounds)


        
    def play(self, mv: int):

        if (mv == PinRock):
            self.rockButton.trigger_action(0.1)
            return
        if (mv == PinPaper):
            self.paperButton.trigger_action(0.1)
            return
        if (mv == PinScissor):
            self.scissorButton.trigger_action(0.1)
            return


        self.display.resColor = [.9, .9, .9, 1]
        self.rpsHandler.train_model()

        oppmv = rpsUtils.get_answer(self.rpsHandler.guess())
        #oppmv = self.rpsHandler.guess()

        self.display.text = str(rpsUtils.translate_move_to_char(oppmv))
        self.rpsHandler.add_round(oppmv, mv)
        
        history = rpsUtils.get_results(self.rpsHandler.p1data, self.rpsHandler.p2data)
        x = range(0, len(history[0])-self.getLenOfInitData())

        if mv - oppmv == 1 or mv - oppmv == -2:
            self.display.resColor = [0,.8,0,1]
        elif mv - oppmv == -1 or mv - oppmv == 2:
            self.display.resColor = [1, .2, .2, 1]

        fig = self.history_diagram.figure
        fig.clear()


        ax = fig.add_subplot(111)  
      
        tmp = history[0][self.getLenOfInitData():] - history[0][self.getLenOfInitData()]
        line1, = ax.plot(x, tmp, 'o-', color='green')
        line1.set_ydata(tmp)

        tmp = history[1][self.getLenOfInitData():] - history[1][self.getLenOfInitData()]
        line2, = ax.plot(x, tmp, 'o-', color='blue')
        line2.set_ydata(tmp)

        tmp = history[2][self.getLenOfInitData():] - history[2][self.getLenOfInitData()]
        line3, = ax.plot(x, tmp, 'o-', color='red')
        line3.set_ydata(tmp)
        
        fig.legend((line1, line2, line3), ('wins', 'ties', 'loss'), 'upper left', fontsize=16)
      
        fig.canvas.flush_events()
        fig.canvas.draw_idle()

        self.set_counter()


        if (self.rpsHandler.num_of_rounds() - len(self.data[0])) > self.rounds:

            GPIO.cleanup()

            if ((history[0][-1] - history[0][self.getLenOfInitData()]) > self.successLimit or (history[1][-1] - history[1][self.getLenOfInitData()]) > self.successLimit) :
                sm.get_screen('result').resultMsg.text = 'Nice game!\nLooks like your\'re cut\n from the same cloth!'
                sm.get_screen('result').success = 1
            else :
                sm.get_screen('result').resultMsg.text = 'Thank you!\nThis seems not to be your sibling in mind.\nBut deversity is a key factor to success.'
                sm.get_screen('result').success = 0

            line1.set_ydata(0)
            line2.set_ydata(0)
            line3.set_ydata(0)
            fig.canvas.draw()

            sm.get_screen('result').opponentName = self.opponentName
            sm.get_screen('result').playerName.text = self.playerName
            sm.current = 'result'


class ChooseScreen(Screen):
    pass

class ResultScreen(Screen):
    success = 0

    def savePlayersData(self):
        with open(bytes("/home/hadoop/Desktop/RockPaperScissor-results/" + self.playerName.text + "-" + str(datetime.datetime.now().timestamp()) + ".txt", encoding='utf8'), 'wb') as fp:
            fp.write(bytes("Name: " + self.playerName.text + "\n", encoding='utf8'))
            fp.write(bytes("Street: " + self.street.text  + "\n", encoding='utf8'))
            fp.write(bytes("Zip: " + self.postalZip.text  + "\n", encoding='utf8'))
            fp.write(bytes("EmailOrOther: " + self.telephone_email_other.text  + "\n", encoding='utf8'))
            fp.write(bytes("Success: " + str(self.success) + "\n", encoding='utf8'))
            fp.write(bytes("Opponent: " + self.opponentName + "\n", encoding='utf8'))
            fp.close()
        success = 0

class OpponentChooserWidget(BoxLayout):
    pass


class ChooseApp(App):

    def build(self):
        self.title = 'RockPaperScissor'
        return sm


if __name__ == "__main__":


    sm = ScreenManager()
    sm.add_widget(ChooseScreen(name='chooser'))
    sm.add_widget(GameScreen(name='game'))
    sm.add_widget(ResultScreen(name='result'))

    Window.fullscreen = False
    app = ChooseApp()
    app.run()


