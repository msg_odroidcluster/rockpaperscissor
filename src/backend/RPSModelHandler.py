import numpy as np
import pandas as pd
# import terminalplot as tp
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
import matplotlib.pyplot as plt
import simplejson
import random as rnd
#from sklearn.metrics import classification_report, confusion_matrix, accuracy_score


class RPSModelHandler:

    def __init__(self, p1moves: np.ndarray = np.array([], dtype=int), p2moves: np.ndarray = np.array([], dtype=int), depth: int = 2, values: list() = [0, 1, 2]):

        self.p1data = p1moves
        self.p2data = p2moves

        self.depth = depth
        
        self.values = values
        self.X_train = pd.DataFrame
        self.X_test = pd.DataFrame
        self.y_train = pd.DataFrame
        self.y_test = pd.DataFrame

        self.classifier = RandomForestRegressor

    def num_of_rounds(self):
        return self.p1data.size

    def add_round(self, p1: int, p2: int):
        self.p1data = np.append(self.p1data, p1)
        self.p2data = np.append(self.p2data, p2)

    def get_colname_list(self, with_label: bool):
        ret = []

        for value in self.values:
            if with_label:
                ret.append("label_" + str(value))
            for x in range(1, self.depth + 1):
                ret.append("p1-" + str(x) + "_" + str(value))
                ret.append("p2-" + str(x) + "_" + str(value))

        return ret

    def train_model(self, forestsize: int = -1):

        if (self.num_of_rounds() > self.depth + 4):
            my_dict = {"label": self.p2data[self.depth:self.num_of_rounds() - 1]}
            for x in range(1, self.depth + 1):
                my_dict["p1-" + str(x)] = self.p1data[self.depth - x:self.num_of_rounds() - (x + 1)]
                my_dict["p2-" + str(x)] = self.p2data[self.depth - x:self.num_of_rounds() - (x + 1)]
    
            df = pd.DataFrame(my_dict)
    
            df = pd.get_dummies(df.astype(str))
    
            self.add_missing_dummy_columns(df, self.get_colname_list(True))
    
            x = df.filter(regex='^p', axis=1).astype(str)
            y = df.filter(regex='^label', axis=1).astype(str)
    
            
            self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(x, y, test_size=0.2)
                
            if (forestsize == -1):
                forestsize = int(np.rint(self.num_of_rounds()/10+self.depth**2))
                
            self.classifier = RandomForestRegressor(n_estimators=forestsize, random_state=0)
            self.classifier.fit(self.X_train, self.y_train)

    def check_model(self):

        y_pred = self.classifier.predict(self.X_test)
        score = self.classifier.score(self.X_test, self.y_test)

        #return score
        #print(self.classifier.feature_importances_)

        feat_importances = pd.Series(self.classifier.feature_importances_, index=self.X_train.columns)
        feat_importances.nlargest(20).plot(kind='barh')
        #plot(feat_importances.nlargest, kind='barh')
        plt.show()
        y_pred = np.around(y_pred)
        for m in range(len(y_pred)):
           print(self.y_test.values[m], y_pred[m])

        # print(confusion_matrix(y_test,y_pred))
        # print(classification_report(y_test,y_pred))
        # print(accuracy_score(self.y_test, y_pred))

    def guess(self, p1: [] = None, p2: [] = None):

        if (self.num_of_rounds() > self.depth + 4):
            if p1 == None:
                p1 = self.p1data[self.num_of_rounds() - (self.depth):self.num_of_rounds()]
            if p2 == None:
                p2 = self.p2data[self.num_of_rounds() - (self.depth):self.num_of_rounds()]
    
            my_dict = {}
            for x in range(0, self.depth):
                my_dict["p1-" + str(x + 1)] = [p1[len(p1) - (x + 1)]]
                my_dict["p2-" + str(x + 1)] = [p2[len(p1) - (x + 1)]]
    
            question = pd.get_dummies(pd.DataFrame(my_dict).astype(str))
            self.add_missing_dummy_columns(question, self.get_colname_list(False))
    
            result = self.classifier.predict(question)
    
            for y in range(len(self.values)):
                if max(result[0]) == result.item(y):
                    return self.values[y]
        else:
                return rnd.randint(min(self.values), max(self.values))
    def add_missing_dummy_columns(self, d, columns):

        missing_cols = set(columns) - set(d.columns)
        for c in missing_cols:
            d[c] = 0
        d.sort_index(axis=1, inplace=True)

    def savegame(self, filename):
        with open(filename, 'w') as fp:
            simplejson.dump( self.p1data.tolist(), fp)
            simplejson.dump( self.p2data.tolist(), fp)
            fp.close()


class RPSUtils:

    @classmethod
    def translate_move_to_int(self, move: str):
        switcher = {
            "R": 0,
            "r": 0,
            "P": 1,
            "p": 1,
            "S": 2,
            "s": 2
        }
        ret = switcher.get(move, "Invalid")
        if ret == "Invalid": raise InvalidMoveError("Invalid move: " + move)
        return ret

    @classmethod
    def translate_move_to_char(self, move: int):
        switcher = {
            0: "Rock",
            1: "Paper",
            2: "Scissor"
        }
        ret = switcher.get(move, "Invalid")
        if ret == "Invalid": raise InvalidMoveError("Invalid move: " + move)
        return ret

    @classmethod
    def get_answer(self, move: int):
        switcher = {
            0: 1,
            1: 2,
            2: 0
        }
        ret = switcher.get(move, "Invalid")
        if ret == "Invalid": raise InvalidMoveError("Invalid move: " + move)
        return ret

    @classmethod
    def get_loss(cls, data):
        if data[0] - data[1] == 1 or data[0] - data[1] == -2:
            return 1
        else:
            return 0

    @classmethod
    def get_wins(cls, data):
        if data[0] - data[1] == -1 or data[0] - data[1] == 2:
            return 1
        else:
            return 0

    @classmethod
    def get_ties(cls, data):
        if data[0] - data[1] == 0:
            return 1
        else:
            return 0

    @classmethod
    def get_results(cls, d1, d2):

        zipped = list(zip(d1, d2))
        wins = np.array([RPSUtils.get_wins(i) for i in zipped]).cumsum()
        ties = np.array([RPSUtils.get_ties(i) for i in zipped]).cumsum()
        loss = np.array([RPSUtils.get_loss(i) for i in zipped]).cumsum()
        return wins, ties, loss

    @classmethod
    def show_history(cls, data):
        wins, = plt.plot(data[0], label='wins')
        ties, = plt.plot(data[1], label='ties')
        loss, = plt.plot(data[2], label='loss')
        plt.legend(handles=[wins, ties, loss])
        plt.xlabel('round')
        plt.show()

class InvalidMoveError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

