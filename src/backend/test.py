
# generate random integer values
from numpy.random import seed
from numpy.random import randint
import numpy as np
from collections import Counter

rounds = 2000
values = randint(0, 3, 20*rounds)
values = np.reshape(values, (rounds,20))

for x in range(1, rounds-1):
    res = Counter(values[0] - values[x])
    print(res[0])
