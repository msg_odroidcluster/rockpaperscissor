import numpy as np
import sys
sys.path.insert(0, './backend/')
import RPSModelHandler as rps_mh



def generate_game(num_of_rounds: int):
    p1data = np.random.randint(3, size=num_of_rounds);
    # Random player
    # p2data = np.random.randint(3, size=num_of_rounds);

    # DAU player

    p2data = np.ones(num_of_rounds, dtype=int)

    # Sequence player
    # p2data = np.array(list(np.arange(0,3))*np.round(num_of_rounds/3).astype(int))[:num_of_rounds]

    # Parrot player
    #p2data = np.insert(p1data, 0, 0)[:num_of_rounds]

    # optional sequence-adjustments
    #for x in range(num_of_rounds):
        # simple adjustments
        # if x % 5 == 0: p2data[x - 1] = 0
        # if x%4 == 1: p2data[x-1]=1
        # if x%5 == 2: p2data[x-1]=2

    return p1data, p2data


def note_move(a: int, b: int):
    global opp
    opp = np.append(opp, a)[1:]
    global plr
    plr = np.append(plr, b)[1:]
    global hits
    global ties
    global lost
    if a - b == 1 or a - b == -2:
        hits += 1;
    elif a == b:
        ties += 1;
    else:
        lost += 1;


def play_generated_game():
    p1, p2 = generate_game(200)
    c1 = rps_mh.RPSModelHandler(p1, p2, depth=5)
    c1.train_model()
    c1.check_model()
    #rps_mh.RPSUtils.show_history(rps_mh.RPSUtils.get_results(c1.p1data, c1.p2data))


def play_human_game(rounds: int = 50):
    p1, p2 = generate_game(200)
    c1 = rps_mh.RPSModelHandler(p1, p2, depth=5)
    for x in range(rounds):
        print("Round " + str(x + 1) + " / " + str(rounds))
        mv = -1
        while int(mv) < min(c1.values) or int(mv) > max(c1.values):
            try:
                mv = int(input("Your move please(" + str(min(c1.values)) + "-" + str(max(c1.values)) + "):"))
            except:
                mv = -1
        c1.train_model()
        oppmv = rps_mh.RPSUtils.get_answer(c1.guess())
            # oppmv = c1.guess()

        note_move(oppmv, mv)
        c1.add_round(oppmv, mv)
        print("My Move: " + str(oppmv) + "\tHitrate: " + str(hits / c1.num_of_rounds() * 100) + "\t" + str(
            ties / c1.num_of_rounds() * 100) + "\t" + str(lost / c1.num_of_rounds() * 100))

    c1.check_model()
    rps_mh.RPSUtils.show_history(rps_mh.RPSUtils.get_results(c1.p1data, c1.p2data))
    #print(find_optimum(c1.p1data, c1.p2data))

    save_bool = 'x'
    while save_bool != 'y' and save_bool != 'n':
        save_bool = input("Save game? (y/n): ")

    if save_bool == 'y':
        with open(input('Enter filename:'), 'w') as fp:
            simplejson.dump( c1.p1data.tolist(), fp)
            simplejson.dump( c1.p2data.tolist(), fp)

def find_optimum(p1data, p2data):
    
    res = {
        'depth': [],
        'forestsize': [],
        'score': []
    }
    
    for x in range(2, 10):
        for y in range(10,100,10):

            c1 = RPSModelHandler(p1data, p2data, depth=x)
            c1.train_model(forestsize=y)
            
            res['depth'].append(x)
            res['forestsize'].append(y)
            res['score'].append(c1.check_model())
    return res

opp = np.zeros(2, dtype=int)
plr = np.zeros(2, dtype=int)

hits = 0
ties = 0
lost = 0

#play_generated_game()
play_human_game(20)
